<?php
namespace setasignCn\Fpdi;


class PdfWatermark
{
    private $text = "";
    private $transparent = 0.3;
    private $image = '';

    /**
     * 图片地址
     * @param $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @param $text 文字
     * @param $transparent 透明度 0-1
     */
    public function SetWatermarkText($text,$transparent)
    {
        $this->text = $text;
        $this->transparent = $transparent;
    }

    /**
     * @param $type 1文字水印 2图水印
     * @param $path  原pdf 地址
     * @param $new_path 新pdf 地址
     * @return mixed
     */
    public function addWatermark($type,$path,$new_path)
    {
        if($type==1){
            $mpdf =  new  \setasignCn\Mpdf\Mpdf();
            $mpdf->SetWatermarkText($this->text, $this->transparent);//水印文字，透明度
            $mpdf->showWatermarkText = true; //开启水印
            //$mpdf->watermarkAngle = '180';//水印角度
            if (preg_match("/([\x81-\xfe][\x40-\xfe])/", $w, $match)) {
                //如果有中文就添加
                $mpdf->watermark_font = 'GB';
            }
            $pageCount = $mpdf->SetSourceFile($path); //读取原始文件页数
            for ($i=1; $i<=$pageCount; $i++) { //循环添加原始文件
                $import_page = $mpdf->ImportPage($i);
                $mpdf->UseTemplate($import_page);
                if ($i < $pageCount)
                    $mpdf->AddPage();
            }
            $mpdf->Output($new_path, 'F'); //保存新文件
        }else{
            $pdf = new \setasign\Fpdi\Fpdi();
            $file = $path;
            //获取页数
            $pageCount = $pdf->setSourceFile($file);
            //遍历所有页面
            for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++){
                //导入页面
                $templateId = $pdf->importPage($pageNo);
                //获取导入页面的大小
                $size = $pdf->getTemplateSize($templateId);
                //创建页面（横向或纵向取决于导入的页面大小）
                if ($size['width'] > $size['height']){
                    $pdf->AddPage('L', array($size['width'], $size['height']));
                }else {
                    $pdf->AddPage('P', array($size['width'], $size['height']));
                }
                //使用导入的页面
                $pdf->useTemplate($templateId);
                $center_x = $size['width']/2 - 40;$center_y = $size['height']/2;
                $pdf->image($this->image, 0, 0,$size['width']);//全屏背景水印

            }
            //I输出output，D下载download，F保存file_put_contents
            return $pdf->Output('F',$new_path,false);

        }
    }
}
