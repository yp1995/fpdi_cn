<?php

namespace setasignCn\Mpdf;


/**
 * mPDF, PHP library generating PDF files from UTF-8 encoded HTML
 *
 * based on FPDF by Olivier Plathey
 *      and HTML2FPDF by Renato Coelho
 *
 * @license GPL-2.0
 */



class Mpdf  extends \Mpdf\Mpdf
{
    /**
     * @var \Mpdf\Writer\BaseWriter
     */
    private $writer;

    function Text($x, $y, $txt, $OTLdata = [], $textvar = 0, $aixextra = '', $coordsys = '', $return = false)
    {
        $this->CurrentFont['sip'] = "";
        $this->CurrentFont['smp'] = "";
        parent::Text($x, $y, $txt, $OTLdata = [], $textvar = 0, $aixextra = '', $coordsys = '', $return = false);
    }


}
